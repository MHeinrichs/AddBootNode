
# AddBootNode

This is a little programm made for the [AIDE](https://gitlab.com/MHeinrichs/AIDE). Its written in C using dice on the Amiga. 
It searches RDB-partitions on an IDE-drive and tries to mount them. In theory it works with other devices than the ide.device but this has never beed tested.

## Parameters

* DEVICE default: ide.device, optional for other devices, but untested
* MINUNIT default: 0 minimum unit to look for (eg if yur CDROM is master)
* MAXUNIT default:10, maxunit to look for, due to a crudee numbering scheme 10 means "Unit2"
* STEP default: 10 bleiben
* FILE default: none Writes the partition table as a mountable list into the specified file
* DRIVEINFO prints the drive info and partition table on the screen
* TEST Writes "Adding partition xyz" instead of mounting found partitions
* FORCE forces mounting of all drives, MINUNIT, MAXUNIT and step must adress only available devices, e.g. *MINUNIT=0 MAXUNIT=0 STEP =0* if only a master is present 
 
 
**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!
